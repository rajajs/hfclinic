# Heart Failure Clinic

Web application for running a clinic for heart failure patients. Still in very early stage of development.

## Design

Built with [flask], [html5 boilerplate] and [bootstrap 4].


## Try

Create virtual environment and install the required libraries

```mkvirtualenv hfclinic```

```pip install -r requirements.txt```

Edit instance/config.py to enter site specific information

Run the server

```python run.py runserver```

An admin user and some demo users will be created on first run.

Now navigate to localhost:8020

Login as 'admin@example.com' with password 'password'

Optionally 'python run.py create_demo_users' to create demo users for all roles

From admin dashboard, create new users and delete the demo users when required.




