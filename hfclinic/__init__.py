from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from flask_admin import Admin

# init app_
app = Flask(__name__, instance_relative_config=True)
app.config.from_object('config')
app.config.from_pyfile('config.py') # config specific to local instance

db = SQLAlchemy(app)
admin = Admin(app)

# jinja customize
app.jinja_env.filters['zip'] = zip # make zip available in templates

# import views at end
from hfclinic.models import User
from hfclinic.admin_views import UserAdmin
from hfclinic import views


# Add Flask-Admin views 
admin.add_view(UserAdmin(User, db.session))



if __name__ == '__main__':
    app.run()
