from flask_security import UserMixin, RoleMixin, Security, \
    SQLAlchemyUserDatastore

from hfclinic import app, db
from hfclinic.utils import *

import datetime

# users
########
# Users and Role models
roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    # __unicode__ is required by Flask-Admin, so we can have human-readable values for the Role when editing a User.    
    # If we were using Python 3, this would be __unicode__ instead.
    def __unicode__(self):
        return self.name

    # __hash__ is required to avoid the exception TypeError: unhashable type: 'Role' when saving a User
    def __hash__(self):
        return hash(self.name)


class User(db.Model, UserMixin):
    id = db.Column(db.Integer(), primary_key=True)
    email = db.Column(db.String(255), unique=True)
    name = db.Column(db.String(255))
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users',
                                               lazy='dynamic'))

    signature = db.Column(db.Unicode(128)) # filename for signature image
    designation = db.Column(db.String(255))
    
    # dynamic function for gravatar
    def avatar(self):
        # size fixed at 128
        # alternative, if gravatar unavailable, is identicon
        return 'http://www.gravatar.com/avatar/%s?d=%s&s=%d'\
            % (md5(self.email.encode('utf-8')).hexdigest(),
               'identicon', 128)            
    
    def __unicode__(self):
        return self.name

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

##########################################
    

class Patient(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    ## personal details 
    name = db.Column(db.String(256))
    sex = db.Column(db.String(16))   # Male / Female / Others
    birth_date = db.Column(db.Date())

    patient_id = db.Column(db.String(32), unique=True) # unique id

    # contact 
    contact_number = db.Column(db.String(16))  # primary phone number
    contact_number2 = db.Column(db.String(16))  # secondary phone number
    email = db.Column(db.String(64))
    address = db.Column(db.Text())
    pincode = db.Column(db.String(6)) # pincode, 6 digits

    education = db.Column(db.String(32)) # No schooling / Primary / Secondary / Graduate
    # socioeconomic_status = db.Column(db.String(32)) #
    per_capita_income = db.Column(db.Integer())
    preferred_language = db.Column(db.String(32))
    
    ## past medical history (Only based on history at enrolment)
    diabetes = db.Column(db.Boolean())
    hypertension = db.Column(db.Boolean())
    renal_insufficiency = db.Column(db.Boolean())
    previous_mi = db.Column(db.Boolean())
    previous_revascularization = db.Column(db.Boolean())
    previous_cva = db.Column(db.Boolean())
    
    diabetes_diagnosis = db.Column(db.Date()) # accurate to month
    hypertension_diagnosis = db.Column(db.Date())
    prev_mi_date = db.Column(db.Date()) # first MI
    heart_failure_date = db.Column(db.Date()) 
    
    medical_history_comments = db.Column(db.Text()) # details of mi / revasc  /prev cva

    # one time medical info (may be edited later, but not usually updated at each visit)
    rhythm = db.Column(db.String(32)) # sinus / paroxysmal a flutter / permanent a flutter / paroxysmal / persistent / permanent af
    qrs_morphology = db.Column(db.String(32))
    qrs_duration = db.Column(db.Integer())
    allergies = db.Column(db.String(256))  # comma separated items
    height = db.Column(db.Integer()) # cms
    
    # backrefs
    clinic_assessments = db.relationship('ClinicAssessment', backref='patient')
    echo_assessments = db.relationship('EchoAssessment', backref='patient')
    counseling_sessions = db.relationship('CounselingSession', backref='patient')
    

    def age(self, at_date=None):
        """
        Age at specified date 
        at_date should be a date object
        if at_date not given, use today instead

        Return age at date as number of days
        """
        if not at_date:
            at_date = datetime.date.today()

        age_days =  (at_date - self.birth_date).days
        return format_age(age_days)

    def __repr__(self):
        return self.name



class ClinicAssessment(db.Model):
    """
    Data collected in a clinic visit

    First such assessment will be initial assessment.
    subsequent will be periodic assessments
    """
    id = db.Column(db.Integer, primary_key=True)
    
    assessment_date = db.Column(db.Date())
    patient_id = db.Column(db.Integer, db.ForeignKey('patient.id'))

    # vitals
    pulse_rate = db.Column(db.Integer()) # in bpm
    systolic_bp = db.Column(db.Integer()) # mm Hg
    diastolic_bp = db.Column(db.Integer())
    jugular_venous_pressure = db.Column(db.Integer()) # cm of water above sternal angle
    
    # physical signs
    weight = db.Column(db.Integer()) # kg
    
    #
    nyha_class = db.Column(db.String(8)) # I, II, III or IV
    six_min_walk = db.Column(db.Integer()) # meters

    comments = db.Column(db.String(256))
    

class EchoAssessment(db.Model):
    """
    Values recorded during periodic echocardiograms
    """
    id = db.Column(db.Integer, primary_key=True)
    
    echo_date = db.Column(db.Date())    
    patient_id = db.Column(db.Integer, db.ForeignKey('patient.id'))

    # measurements (all in cm)
    ivsd = db.Column(db.Float) 
    pwd = db.Column(db.Float)
    lvedd = db.Column(db.Float)
    lvesd = db.Column(db.Float)
    lvef = db.Column(db.Float)
    
    # valves
    # any stenosis other than aortic can go into comments
    ar = db.Column(db.String(16))
    a_gdt = db.Column(db.Float, default=0)
    mr = db.Column(db.String(16))
    tr = db.Column(db.String(16))
    
    # pressures
    rvsp = db.Column(db.Float)

    # 
    comments = db.Column(db.String(512))

    # calculated values - calculated / recalculated at creation and at edit
    
    # teicholz for volume measurements
    lvesv = db.Column(db.Float)
    lvedv = db.Column(db.Float)
    calculated_ef = db.Column(db.Float)
    
    # ASE cube formula or Devreaux formula for LV mass
    # LVmass( g)0.80[1.04{(PWTLVIDSWT)3LVID3}]0.6
    lv_mass = db.Column(db.Float)

    # body surface area at measurement - use for indexing measurements
    bsa_on_echo_date = db.Column(db.Float)
    
    
    
class CounselingSession(db.Model):
    """
    Components completed during counseling and evaluation of compliance
    """
    id = db.Column(db.Integer, primary_key=True)
    
    counseling_date = db.Column(db.Date())
    patient_id = db.Column(db.Integer, db.ForeignKey('patient.id'))
    
    lifestyle_counseling_done = db.Column(db.Boolean())
    diet_counseling_done = db.Column(db.Boolean())    
    weight_counseling_done = db.Column(db.Boolean())
    medications_counseling_done = db.Column(db.Boolean())    

    lifestyle_counseling_compliance = db.Column(db.String(32))
    diet_counseling_compliance = db.Column(db.String(32))    
    weight_counseling_compliance = db.Column(db.String(32))
    medications_counseling_compliance = db.Column(db.String(32))

    comments = db.Column(db.String(256))    
