
from flask_wtf import FlaskForm as Form
from wtforms import StringField, BooleanField,\
     SelectField, DateField, FloatField, IntegerField, FormField
# from wtforms_components import TimeField
from wtforms.validators import InputRequired, \
    Length, Optional, Regexp, ValidationError, DataRequired, Email
from wtforms.widgets import TextArea

from hfclinic import app
from hfclinic.models import *
from hfclinic.utils import *

from datetime import date

# uniform string format to use for dates
date_str_format = "%d-%m-%Y"


class PatientEnrolmentForm(Form):
    name = StringField("Name", validators=[InputRequired()])
    sex = SelectField("Sex",
                      choices=[(x, x) for x in
                               ['Male', 'Female', 'Other']])
    birth_date = DateField("Date of Birth")  # dont use fixed format
                           # format = date_str_format)
    age = StringField("Age")
    patient_id = StringField(app.config['CENTER_PATIENT_ID']) # each center may have different name for unique id

    contact_number = StringField("Contact number")
    contact_number2 = StringField("Sec contact number")
    email = StringField("email id")
    address = StringField("Address",
                          widget=TextArea())
    pincode = StringField("Pincode", validators=[Length(min=6, max=6, message='Must be exactly 6 digits')]) 
                          

    education = SelectField("Education",
                            choices=[(x,x) for x in
                                     ['', 'Illiterate', 'School (incomplete or complete)',
                                      'College (incomplete or complete)']])
    per_capita_income = IntegerField('Income (annual family income / number of family members in Rupees)')
    preferred_language = SelectField('Preferred Language',
                                     choices=[(x, x) for x in
                                              ['', 'Tamil', 'English', 'Hindi', 'Malayalam',
                                               'Bengali', 'Telugu', 'French',]
                                     ])

class PatientMedicalForm(Form):
    diabetes = BooleanField('Diabetes')
    hypertension = BooleanField('Hypertension')
    renal_insufficiency = BooleanField('Renal insufficiency')
    previous_mi = BooleanField('Previous MI')
    previous_revascularization = BooleanField('Previous revascularization')
    previous_cva = BooleanField('Previous CVA')

    diabetes_diagnosis = DateField('Time of diabetes diagnosis', format='%m-%Y')
    hypertension_diagnosis = DateField('Time of hypertension diagnosis', format='%m-%Y')
    prev_mi_date = DateField('Time of previous MI', format='%m-%Y')
    heart_failure_date = DateField('Time of heart failure diagnosis', format='%m-%Y')
    medical_history_comments = StringField('Comments')
    rhythm = SelectField('Rhythm',
                         choices=[(x, x) for x in
                                  ['', 'Sinus', 'Paroxysmal AF', 'Persistent AF', 'Permanent AF',
                                   'Paroxysmal A flutter', 'Persistent A flutter', 'Others']
                         ])
    qrs_morphology = SelectField('QRS morphology',
                                 choices=[(x, x) for x in
                                          ['', 'Narrow', 'RBBB', 'LBBB', 'IVCD']])
    qrs_duration = IntegerField('QRS duration (ms)')
    allergies = StringField('Allergies')
    height = IntegerField('Height (cm)')


class ClinicAssessmentForm(Form):
    assessment_date = DateField('Assessment date', default=date.today)
    #patient_id
    pulse_rate = IntegerField('Pulse rate (bpm)')
    systolic_bp = IntegerField('Systolic BP (mm Hg)')
    diastolic_bp = IntegerField('Diastolic BP (mm Hg)')
    jugular_venous_pressure = IntegerField('Jugular venous pressure (mean height in cms, above sternal angle)',
                                           validators=[Optional()])
    weight = IntegerField('Weight (kgs)', validators=[Optional()])
    nyha_class = SelectField('NYHA class',
                             choices=[(x, x) for x in ['I', 'II', 'III', 'IV']])
    six_min_walk = IntegerField('Six minute walk distance (m)',
                                           validators=[Optional()])                                
    comments = StringField('Comments', widget=TextArea())


class EchoAssessmentForm(Form): 
    echo_date = DateField('Date of Echo', default=date.today)
    #patient_id 

    ivsd = FloatField('IVS thickness diastole (cm)', validators=[Optional()])
    pwd = FloatField('PW thickness diastole (cm)', validators=[Optional()])
    lvedd = FloatField('LV end diastolic dimension (cm)', validators=[Optional()]) 
    lvesd = FloatField('LV end systolic dimension (cm)', validators=[Optional()])
    lvef = FloatField('LVEF (%)') 
    
    # valves
    # any stenosis other than aortic can go into comments
    ar = SelectField('Aortic regurgitation',
                     choices=[(x,x) for x in ['None', 'Mild', 'Moderate', 'Severe']])
    a_gdt = FloatField('Mean aortic gradient (mm Hg)')
    mr = SelectField('Mitral regurgitation',
                     choices=[(x,x) for x in ['None', 'Mild', 'Moderate', 'Severe']])
    tr = SelectField('Tricuspid  regurgitation',
                     choices=[(x,x) for x in ['None', 'Mild', 'Moderate', 'Severe']])
    
    # pressures
    rvsp = FloatField('RV systolic pressure', validators=[Optional()])

    # 
    comments = StringField('Comments', widget=TextArea(), validators=[Optional()])
    

class CounselingSessionForm(Form):
    counseling_date = DateField('Counseling date', default=date.today)

    lifestyle_counseling_done = BooleanField('Lifestyle counseling done')
    diet_counseling_done = BooleanField('Diet counseling done')
    weight_counseling_done = BooleanField('Weight counseling done')
    medications_counseling_done = BooleanField('Medications counseling done')

    lifestyle_counseling_compliance = SelectField('Lifestyle compliance',
                                                  choices=[(x,x) for x in ['NA', 'Poor', 'Good', 'Very good']])
    diet_counseling_compliance = SelectField('Diet compliance',
                                                  choices=[(x,x) for x in ['NA', 'Poor', 'Good', 'Very good']])
    weight_counseling_compliance = SelectField('Weight compliance',
                                                  choices=[(x,x) for x in ['NA', 'Poor', 'Good', 'Very good']])
    medications_counseling_compliance = SelectField('Medications compliance',
                                                  choices=[(x,x) for x in ['NA', 'Poor', 'Good', 'Very good']])

    comments = StringField('Comments', widget=TextArea())
    
