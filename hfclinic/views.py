
# python imports
from datetime import datetime, date
import time

# flask imports
from flask import render_template, redirect, url_for, request, flash
from flask_security import roles_accepted, login_required
from werkzeug.utils import secure_filename

# internal imports
from hfclinic.models import *
from hfclinic.forms import *
from hfclinic.utils import *


#############################
## view functions
#############################

# home page
@app.route('/')
def home():
    n_patients = Patient.query.count()
    
    return render_template('views/home.html',
                           n_patients=n_patients)


# patient
# --------

# add patient 
@app.route('/patient/add', methods=['GET', 'POST'])
@roles_accepted('consultant', 'resident', 'nurse', 'data_entry_operator')
def add_patient():
    """
    present form to enter details and register patient
    uniqueness of patient id checked on form submit
    on adding patient, redirect to patient page
    """
    form = PatientEnrolmentForm()

    if form.validate_on_submit():
        # check if patient id already exists
        patient_id = form.data['patient_id']
        exists = Patient.query.\
                 filter(Patient.patient_id == patient_id).\
                 first()
        if exists:
            flash('Patient with same id already exists', 'warning')
            return render_template('forms/patient_form.html',
                                   form=form, action='add')
        
        patient = Patient()
        form.populate_obj(patient) 
        db.session.add(patient)
        db.session.commit()

        flash('Patient added', 'warning')

        return redirect(url_for('view_patient', id = patient.id))

    return render_template('forms/patient_form.html',
                           form=form,
                           action='add')


# edit patient
@app.route('/patient/edit/<int:id>', methods=['GET', 'POST'])
@roles_accepted('consultant', 'resident', 'nurse', 'data_entry_operator')
def edit_patient(id):
    # present form with the patient details
    # store the returned edits
    patient = Patient.query.get(id)
    form = PatientEnrolmentForm(obj = patient)

    if form.validate_on_submit():
        form.populate_obj(patient)
        db.session.commit()
        return redirect(url_for('view_patient', id=patient.id))

    return render_template('forms/patient_form.html',
                           form=form,
                           patient=patient,
                           action='edit')


# delete patient
@app.route('/patient/delete/<int:id>', methods=['GET', 'POST'])
@roles_accepted('consultant', 'resident')
def delete_patient(id):
    patient = Patient.query.get(id)
    if patient:
        db.session.delete(patient)
        db.session.commit()

    return redirect(url_for('home'))


# find patient
@app.route('/patient/find')
@roles_accepted('consultant', 'resident', 'nurse', 'data_entry_operator')
def find_patient():
    # form with single field
    # enter part of name / phone numnber / hosp no to find patient
    # ajax to process search
    return render_template('views/patients/find_patient.html')


# list all patients with pagination, sort and search
@app.route('/patient/list_patients')
@roles_accepted('consultant', 'resident', 'nurse', 'data_entry_operator')
def list_patients():
    # use datatables to list patients and search / sort
    patients = Patient.query.all()
    return render_template('views/patients/list_patients.html',
                           patients=patients)    


# medical history - one time
@app.route('/patient/edit_medical/<int:id>', methods=['GET', 'POST'])
@roles_accepted('consultant', 'resident')
def edit_patient_medical(id):
    patient = Patient.query.get(id)
    form = PatientMedicalForm(obj = patient)
    
    if form.validate_on_submit():
        form.populate_obj(patient)
        db.session.commit()
        return redirect(url_for('view_patient', id=patient.id))

    return render_template('forms/patient_medical_form.html',
                           form=form,
                           patient=patient)


# periodic clinic visit
@app.route('/patient/add_clinic_assessment/<int:id>', methods=['GET', 'POST'])
@roles_accepted('consultant', 'resident', 'nurse')
def add_clinic_assessment(id):
    patient = Patient.query.get(id)
    form = ClinicAssessmentForm()

    if form.validate_on_submit():
        ca = ClinicAssessment(patient_id = patient.id)
        form.populate_obj(ca)
        db.session.add(ca)
        db.session.commit()
        return redirect(url_for('view_patient', id=patient.id))

    return render_template('forms/clinic_assessment_form.html',
                           form=form, patient=patient)


@app.route('/patient/delete_last_clinic_assessment/<int:id>')
@roles_accepted('consultant', 'resident', 'nurse')
def delete_last_clinic_assessment(id):
    patient = Patient.query.get(id)
    ca = patient.clinic_assessments
    if len(ca) > 0:
        ca.sort(key=lambda x: x.assessment_date)
        db.session.delete(ca[-1])
        db.session.commit()
    return redirect(url_for('view_patient', id=patient.id))        
        

# periodic echos
@app.route('/patient/add_echo_assessment/<int:id>', methods=['GET', 'POST'])
@roles_accepted('consultant', 'resident')
def add_echo_assessment(id):
    patient = Patient.query.get(id)
    form = EchoAssessmentForm()

    if form.validate_on_submit():
        ea = EchoAssessment(patient_id = patient.id)
        form.populate_obj(ea)
        
        # calculated values
        ca = patient.clinic_assessments

        # bsa - ideally use last ht and wt values before the echo - for now using most current value
        # this may be wrong when entering an old echo report
        if len(ca) > 0:
            ca.sort(key=lambda x: x.assessment_date)
            wt = ca[-1].weight # in kg
            ht = patient.height # in cms
            ea.bsa = calculate_bsa(wt, ht)
        
        # volumes - use teicholz
        ea.lvedv = calculate_lv_volume(ea.lvedd)
        ea.lvesv = calculate_lv_volume(ea.lvesd)
        
        # mass - use devreaux formula
        ea.lv_mass = calculate_lv_mass(ea.ivsd, ea.pwd, ea.lvedd) 
        
        db.session.add(ea)
        db.session.commit()
        return redirect(url_for('view_patient', id=patient.id))

    return render_template('forms/echo_assessment_form.html',
                           form=form, patient=patient)


@app.route('/patient/delete_last_echo_assessment/<int:id>')
@roles_accepted('consultant', 'resident')
def delete_last_echo_assessment(id):
    patient = Patient.query.get(id)
    ea = patient.echo_assessments
    
    if len(ea) > 0:
        ea.sort(key=lambda x: x.echo_date)
        db.session.delete(ea[-1])
        db.session.commit()
    return redirect(url_for('view_patient', id=patient.id))

        
# counseling - periodic
@app.route('/patient/add_counseling_session/<int:id>', methods=['GET', 'POST'])
@roles_accepted('consultant', 'resident', 'nurse')
def add_counseling_session(id):
    patient = Patient.query.get(id)
    form = CounselingSessionForm()

    if form.validate_on_submit():
        cs = CounselingSession(patient_id = patient.id)
        form.populate_obj(cs)
        db.session.add(cs)
        db.session.commit()

        return redirect(url_for('view_patient', id=patient.id))

    return render_template('forms/counseling_session_form.html',
                           form=form, patient=patient)    
    
    
@app.route('/patient/add_photo')
def add_patient_photo():
    """
    Form to upload photograph of patient
    Should use device camera if mobile device used
    Should allow use of webcam for pc
    client side cropping and resizing needed
    """
    #TODO:
    # set up form with proper enctype    
    
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('uploaded_file',
                                    filename=filename))    


# patient details display
@app.route('/patients/view/<int:id>')
@roles_accepted('consultant', 'resident', 'nurse', 'data_entry_operator')
def view_patient(id):
    patient = Patient.query.get(id)

    # clinic assessments
    if patient:
        # values measured at each clinic visit are collected as a dict
        ca = patient.clinic_assessments
        ca.sort(key=lambda x: x.assessment_date)

        ca_dict = {}
        for param in ['assessment_date', 'pulse_rate', 'systolic_bp', 'diastolic_bp',
                      'jugular_venous_pressure', 'weight', 'nyha_class',
                      'six_min_walk', 'comments']:
            ca_dict[param] = [assessment.__dict__[param] for assessment in ca]

        ca_dict['assessment_date_str'] = [dt.strftime('%m-%y') for dt in ca_dict['assessment_date']]
        ca_dict['assessment_date_timestamp'] = [time.mktime(dt.timetuple()) for dt in ca_dict['assessment_date']] 

        # counseling sessions
        cs = patient.counseling_sessions
        cs.sort(key=lambda x: x.counseling_date)

        # echo assessments
        echo_assessments = patient.echo_assessments
        echo_assessments.sort(key=lambda x: x.echo_date)
        
        return render_template('views/patients/view_patient.html',
                               patient=patient, ca_dict = ca_dict, cs=cs,
                               echo_assessments = echo_assessments)
    
    else:
        flash('Patient not found', 'warning')
        return redirect(url_for('home'))


#############################
## non view functions
#############################


@app.route('/patient/find_patient_from_search_str', methods=['POST'])
def find_patient_from_search_str():
    """
    Receive search string by ajax
    
    string maybe 
      - name
      - patient id
      - phone no
    """
    def search_by_name(string):
        pts = Patient.query.\
              filter(Patient.name.like('%'+string+'%')).\
              all()
        return pts

    def search_by_patient_id(string):
        pts = Patient.query.\
              filter(Patient.patient_id.contains(string)).\
              all()
        return pts

    def search_by_phone_no(string):
        pts = Patient.query.\
              filter(Patient.contact_number.contains(string)).\
              all()
        return pts


    search_string = request.json['search_string']

    # search name if string of len > 3 is all characters
    if search_string.isalpha():
        pts = search_by_name(search_string)
        
    # search name if string of len > 3 is all characters
    if search_string.isalpha():
        pts = search_by_name(search_string)
    
    # search phone no and hosp no if string is all numbers
    elif search_string.isdigit():
        patient_id_matches = search_by_patient_id(search_string)
        phone_no_matches = search_by_phone_no(search_string)

        pts = patient_id_matches + phone_no_matches
        
    # search hosp_no
    else:
        pts = search_by_hosp_no(search_string)
    
    return render_template('ajax_templates/patient_list.html',
                               patients=pts)

    
