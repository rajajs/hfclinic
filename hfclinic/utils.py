
def format_age(age_in_days):
    """
    Given age as number of days
    return formatted string
    
    For less than a month, reported as days only
    for less than a year, reported as months and days
    for more than a year, reported as yrs and months
    for more than 10 years, only report as yrs
    """
    #TODO: change formatting for single / plural (eg. 1 yr instead of yrs)
    
    if age_in_days < 31:
        return '%d days' % age_in_days

    elif age_in_days < 366:
        return '%d mths %d days' % (age_in_days / 30.5,
                                    age_in_days % 30.5)

    elif age_in_days < 3652:
        return '%d yrs %d mths' % (age_in_days / 365.25,
                                   (age_in_days % 365.25) / 30)

    else:
        return '%d yrs' % round(age_in_days / 365.25)



def calculate_bsa(wt, ht):
    """
    Given wt in kg and 
          ht in cm
    return body surface area in m2

    uses dubois and dubois formula
    """
    # Dubois and Dubois = 0.007184 x (patient height in cm)0.725 x (patient weight in kg)0.425
    return 0.007184 * (ht ** 0.725) * (wt ** 0.425)
    

def calculate_lv_volume(dim):
    """
    Given dimension in cm
    calculate vol in ml
    """
    # Teicholz (V=7.0XLVID3/(2.4+LVID))
    if dim == None:
        return None

    return 7 * (dim ** 3) / (2.4 + dim)


def calculate_lv_mass(ivsd, pwd, lvedd):
    """
    Given ivs thickness
          pw thickness and
          edd
    return lv mass in g
    """
    print 'calculating lv mass'
    print 'values are'
    print ivsd, pwd, lvedd
    
    if None in [ivsd, pwd, lvedd]:
        return None
    else:
        return (0.8 * (1.4 *  ((ivsd + pwd + lvedd) ** 3) - (lvedd ** 3))) + 0.6 

    
def tests():
    # format_age
    test_set = [(30, '30 days'),
                (72, '2 mths, 11 days'),
                (3651, '9 yrs 11 mths'),
                (7308, '20 yrs')]
    for n_days, expected_age in test_set:
        age = format_age(n_days)
        print "age in days:", n_days
        print "expected:", expected_age
        print "got:", age
        print "-----------------\n"

    print calculate_bsa(65, 180)
    print 'BSA expected', '1.82779323855'
        
    print calculate_lv_volume(3.5)
    print 'LV volume expected', 50.8686440678


    print calculate_lv_mass(1.3, 1.4, 5.4)
    print 'LV mass expected', 312
    
if __name__ == "__main__":
    tests()
