import os
import datetime

basedir = os.path.abspath(os.path.dirname(__file__))


# DEBUG on for development
DEBUG = True
MAINTENANCE = False

# main db
SQLALCHEMY_DATABASE_URI = 'sqlite:///' +\
                          os.path.join(basedir,
                                       'hfclinic.db')

SQLALCHEMY_TRACK_MODIFICATIONS = False


# file uploads
UPLOAD_FOLDER = os.path.join(basedir, '/static/img/uploaded_images')

# variables to be stored in instance/config.py
# these values will be overridden
SECRET_KEY = '' 
CENTER_PATIENT_ID = 'Hospital No.'
CENTER_CLINIC_NAME = "Heart failure clinic"
CENTER_HOSPITAL_NAME = "Demo Hospital"
CENTER_LOGO = ""

# roles for users. list of tuples (rolename, description)
# admin role is essential
ROLES = [('Administrator', '')]

SECURITY_PASSWORD_HASH = 'bcrypt'
SECURITY_PASSWORD_SALT = 'abcdefgh'  # modify in instance
