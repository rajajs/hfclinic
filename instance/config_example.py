
SECRET_KEY = '89fhtuaofpfpgakalflf'

CENTER_PATIENT_ID = 'Hosp No.'
CENTER_CLINIC_NAME = "Heart failure clinic"
CENTER_HOSPITAL_NAME = "Demo Hospital"
CENTER_LOGO = ""

# dont remove admin. Others can be removed / modified / added
ROLES = [
    ('admin', 'Admininstrator'),
    ('consultant', 'Consultant'),
    ('resident', 'Resident doctors'),
    ('nurse', 'nurse'),
    ('social_worker', 'Social worker'),
    ('dietitian', 'Dietitian'),
    ('data_entry_operator', 'Data Entry Operator')
]

SECURITY_PASSWORD_SALT = 'ckfof88102jfrnfmf,fl;'

