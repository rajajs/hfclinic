from flask_script import Manager, Server
from flask_migrate import Migrate, MigrateCommand

from flask_security.utils import encrypt_password

from hfclinic import app, db
from hfclinic.models import User, user_datastore, Role

manager = Manager(app)
migrate = Migrate(app, db)

# runserver
port = 8020
manager.add_command('runserver', Server(host='0.0.0.0', port=port))
manager.add_command('db', MigrateCommand)


@app.before_first_request
def on_first_run():
    """
    """
    # create db
    db.create_all()

    # create roles 
    for rolename, description in app.config['ROLES']:
        user_datastore.find_or_create_role(name=rolename, description=description)
        print 'Adding role if it doesnt exist', rolename
        
    db.session.commit()
    print 'Created roles'

    # create first admin user
    encrypted_password = encrypt_password('password')
    if not user_datastore.get_user('admin@example.com'):
        user_datastore.create_user(email='admin@example.com', name='Admin', password=encrypted_password)
        user_datastore.add_role_to_user('admin@example.com', 'admin')
    db.session.commit()
        

@manager.command
def create_demo_users():
    # create demo users
    encrypted_password = encrypt_password('password')    
    for rolename, _ in app.config['ROLES']:
        if rolename != 'admin': 
            demo_username = 'demo_' + rolename + '@example.com'
            if not user_datastore.get_user(demo_username):
                user_datastore.create_user(email=demo_username, name='Demo '+rolename, password=encrypted_password)
                user_datastore.add_role_to_user(demo_username, rolename)
            print 'Added user', demo_username
    db.session.commit()


@manager.command
def create_user(email, name, password, rolename):
    encrypted_password = encrypt_password(password)
    if not user_datastore.get_user(email):
        user_datastore.create_user(email=email, name=name, password=encrypted_password)
        user_datastore.add_role_to_user(email, rolename)
        print 'Added user', email
        db.session.commit()


        
if __name__ == "__main__":
    manager.run()
